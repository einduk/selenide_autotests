/**
 * Открыть сайт ДНС
 * С помощью левого меню перейти по пути Смартфоны – Смартфоны – Смартфоны 2019 года
 * С помощью радиобаттона ограничить сумму
 * С помощью фильтра установить «Производителя» = «Apple»
 * Нажать кнопку «Показать» которая всплывает при установке фильтра
 * Отсортировать по убыванию цены
 * Перейти к одному из вариантов выборки
 * С помощью выпадающего списка «Доп. Гарантия» выбрать дополнительную гарантию на 1 год
 * Удостовериться, что цена на смартфон изменилась, вывести в консоль цену гарантии.
 */
package ru.ShopDns.tests3.checkingPriceWhithGuarantee;

import org.junit.jupiter.api.Test;
import ru.ShopDns.commonStepsForTests.CommonStep;

public class CheckingPriceWithGuaranteeTest extends CommonStep {
    /**
     * Test check a product price when we use guarantee
     */
    @Test
    void testSmartphoneComparison() {
        CommonStep.openSite();
        CommonStep.followingLinks("Смартфоны и гаджеты", "Смартфоны");
        smartphonesPage.scrollAndClickOnClosedFilterList("//div[@data-block-id='f[pqc]']", "//div[@data-id='f[pqc]']");
        smartphonesPage.selectVisibleFilterElement("2019");
        smartphonesPage.scrollToVisibleBlock("//div[@data-id='brand']");
        smartphonesPage.selectVisibleFilterElement("Apple");
        smartphonesPage.scrollToVisibleBlock("//div[@data-block-id='price']");
        smartphonesPage.selectVisibleFilterElement("40 001");
        smartphonesPage.clickShowButton();
        smartphonesPage.clickSortingTypeSelection();
        smartphonesPage.selectRandomIphone();
        checkingPriceWithGuaranteePage.checkingResultingCost();
    }

}
