/**
 * Открыть сайт ДНС
 * С помощью левого меню перейти по пути Смартфоны – Смартфоны – Смартфоны 2020 года
 * С помощью радиобаттона ограничить сумму
 * С помощью фильтра установить «Производителя» = «Xiaomi»
 * С помощью фильтра установить «Объем встроенной памяти» =  «64» и «128»
 * Нажать кнопку «Применить»
 * Удостовериться, что выборка соответствует заданным фильтрам
 * Для запуска кейса через консоль выполнить mvn test -Dtest=ClassName
 */
package ru.ShopDns.tests3.checkingSpecifiedFilters;

import com.thoughtworks.gauge.Step;
import org.junit.jupiter.api.Test;
import ru.ShopDns.commonStepsForTests.CommonStep;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.CollectionCondition.*;

public class CheckingSpecifiedFiltersTest extends CommonStep {
    @Test
    void testCheckingFilters() {
        @Step("Переход на главную страницу DNS")
        CommonStep.openSite();
        CommonStep.followingLinks("Смартфоны и гаджеты", "Смартфоны", "2020 года");
        smartphonesPage.scrollToVisibleBlock("//div[@data-block-id='price']");
        smartphonesPage.selectVisibleFilterElement("18 001 - 27 000");
        smartphonesPage.scrollToVisibleBlock("//div[@data-id='brand']");
        smartphonesPage.selectVisibleFilterElement("Xiaomi");
        smartphonesPage.scrollAndClickOnClosedFilterList("//div[@data-id='f[9a9]']");
        smartphonesPage.selectVisibleFilterElement("64");
        smartphonesPage.selectVisibleFilterElement("128");
        smartphonesPage.clickConfirmButton();
        $$("span.picked-filter").filter(visible).shouldHave(size(5));
    }
}