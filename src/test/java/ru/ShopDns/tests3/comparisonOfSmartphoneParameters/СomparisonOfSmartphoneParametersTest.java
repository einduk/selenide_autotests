/**
 * Открыть сайт ДНС
 * С помощью левого меню перейти по пути Смартфоны – Смартфоны – Смартфоны 2019 года
 * С помощью радиобаттона ограничить сумму
 * Добавить 2 телефона к сравнению
 * Перейти в раздел «Сравнение»
 * С помощью таба(переключателя) выбрать режим «Только различающиеся параметры»
 * Удостовериться, что перестали отображаться одинаковые параметры
 * Для запуска кейса через консоль выполнить mvn test -Dtest=ClassName
 */
package ru.ShopDns.tests3.comparisonOfSmartphoneParameters;

import org.junit.jupiter.api.Test;
import ru.ShopDns.commonStepsForTests.CommonStep;

import static com.codeborne.selenide.Selenide.open;

public class СomparisonOfSmartphoneParametersTest extends CommonStep {
    /**
     * Test check of the display of individual parameters of two selected smartphones
     */
    @Test
    void testSmartphoneComparison() {
//        CommonStep.openSite();
//        CommonStep.followingLinks("Смартфоны и гаджеты", "Смартфоны");
        open("https://www.dns-shop.ru/catalog/17a8a01d16404e77/smartfony/");
        smartphonesPage.scrollAndClickOnClosedFilterList("//div[@data-block-id='f[pqc]']", "//div[@data-id='f[pqc]']");
        smartphonesPage.selectVisibleFilterElement("2019");
        smartphonesPage.scrollToVisibleBlock("//div[@data-block-id='price']");
        smartphonesPage.selectVisibleFilterElement("18 001");
        smartphonesPage.clickConfirmButton();
        smartphonesPage.findRandomSmartphonesForCompare();
        smartphonesPage.clickOnCompareButton();
//        comparePage.clickOnShowDifferent();
        comparePage.findTextInCells();
    }
}
