package ru.ShopDns.pages3.compareSmartphones;

import lombok.var;

import static com.codeborne.selenide.Selenide.*;

public class ComparePage {
    private static final String showDifferentToggle = "//div//span[@class='base-ui-toggle__icon']";
    private static final String featureListValue = "//div[@class='group-table__option-wrapper']//p[@class = 'group-table__product-value']";

    /** Convert the list into a text array and check that the elements do not match
     */
    public void findTextInCells() {
        var value = new Object() {
            int currentValue = 1;
        };
        $$(featureListValue).forEach(selenideElement -> {
            String targetValue = "";
            if (value.currentValue % 2 != 0) {
                targetValue = selenideElement.getAttribute("OtherText");
            } else {
                selenideElement.getAttribute("otherText").equals(targetValue);
            }
            value.currentValue += 1;
        });
    }

    public void clickOnShowDifferent() {
        $x(showDifferentToggle).scrollIntoView(false);
        $x(showDifferentToggle).click();
    }


}
