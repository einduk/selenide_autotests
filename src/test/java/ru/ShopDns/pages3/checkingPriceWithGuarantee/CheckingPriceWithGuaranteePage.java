package ru.ShopDns.pages3.checkingPriceWithGuarantee;

import com.codeborne.selenide.Condition;

import static com.codeborne.selenide.Selenide.*;

public class CheckingPriceWithGuaranteePage {
    private static final String cost = "//div[@class='price-block']//span[contains(@class,'product-card-price__current')]";
    private static final String select = "//div[@class='col-header col-dns-recommends']//select[contains(@class,'product-warranty__select')]";

    private String getCostSmartphone() {
        return $x(cost).getText();
    }

    private void chooseAddGuarantee(String index) {
        $x(select).selectOptionByValue(index);
    }

    public void checkingResultingCost() {
        String costText = this.getCostSmartphone();
        System.out.println("cost = " + costText);
        this.chooseAddGuarantee("0");
        String costWithGuarantee = this.getCostSmartphone();
        System.out.println("Cost with guarantee = " + costWithGuarantee);
        $x(cost).shouldNotHave(Condition.exactText(costText));
    }
}
