package ru.ShopDns.pages3.Smartphone;

import com.codeborne.selenide.ClickOptions;
import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.ex.ListSizeMismatch;

import java.util.Random;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byLinkText;
import static com.codeborne.selenide.Selenide.*;

public class SmartphonesPage {
    private static final String locationConfirmButton = "//button[@data-role='filters-submit']";
    private static final String locationShowButton = "//div[@class='apply-filters-float-btn']";
    private static final String locationCompareButton = "//div[@class='compare-popover__buttons']//a[@class='button-ui button-ui_brand']";
    private static final String locationSortingType = "//a//span[@class='top-filter__icon']";
    private static final String locationProductList = "//div[@data-id='product']//a[@class='catalog-product__name ui-link ui-link_black']";
    private static final String locationCheckboxesCompareOnPage = "//label[span='Сравнить']//span[@class='ui-checkbox__box']";
    private static final String locationCostFilter = "//div[@class='ui-checkbox-group ui-checkbox-group_list']//span";

    public void scrollToVisibleBlock(String block) {
        $x(block).scrollTo();
        String sub = "brand";
        if (block.indexOf(sub) != -1) {
            $(byLinkText("Показать всё")).click();
        }
    }

    public void selectVisibleFilterElement(String elementFilter) {
        $$x(locationCostFilter).findBy(text(elementFilter)).click();
    }

    public void scrollAndClickOnClosedFilterList(String... args) {
        try {
            $x(args[0]).scrollIntoView("{behavior: \"auto\", block: \"center\", inline: \"center\"}").shouldBe(visible).click();
        } catch (com.codeborne.selenide.ex.ElementNotFound n) {
            $x(args[1]).click();
        }
    }

    public void clickConfirmButton() {
        $x(locationConfirmButton).click();
    }

    public void selectRandomIphone() {
        ElementsCollection smartphones = $$x(locationProductList);
        System.out.println(smartphones.size());
        if (smartphones.size() > 1) {
            SelenideElement randomSmartphones = smartphones.get(new Random().nextInt(smartphones.size()));
            randomSmartphones.scrollIntoView(false);
            randomSmartphones.click();
        }
    }

    /**
     * Scrolling the page with a list of smartphones to select two random models
     */

    public void findRandomSmartphonesForCompare() {
        try {
            $$x("//div[@data-id='product']").shouldBe(CollectionCondition.empty.because(
                    "Странно, но ничего нет. Попробуйте изменить критерии поиска"));
        } catch (ListSizeMismatch error) {
            ElementsCollection checkboxCompare = $$x(locationCheckboxesCompareOnPage);
//            System.out.println(checkboxCompare);
            int j = 0;
            if (checkboxCompare.size() > 1) {
                while (j < 2) {
                    SelenideElement randomCheckbox = checkboxCompare.get(new Random().nextInt(checkboxCompare.size()));
                    randomCheckbox.click();
                    j++;
                }
            } else {
                System.out.println("Критериям поиска соответствует только 1 значение");
            }
        }
    }

    public void clickOnCompareButton() {
        $x(locationCompareButton).click();
    }

    public void clickShowButton() {
        $x(locationShowButton).click();
    }

    public void clickSortingTypeSelection() {
        $x(locationSortingType).click();
        $x("//span[contains(., 'По убыванию цены')]").click();
    }
}
