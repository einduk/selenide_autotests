package ru.ShopDns.commonStepsForTests;

import com.codeborne.selenide.Configuration;
import ru.ShopDns.pages3.checkingPriceWithGuarantee.*;
import ru.ShopDns.pages3.compareSmartphones.*;
import ru.ShopDns.pages3.Smartphone.*;

import static com.codeborne.selenide.Selectors.byLinkText;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.open;

public class CommonStep {
    public SmartphonesPage smartphonesPage = new SmartphonesPage();
    public CheckingPriceWithGuaranteePage checkingPriceWithGuaranteePage = new CheckingPriceWithGuaranteePage();
    public ComparePage comparePage = new ComparePage();

    public static void openSite() {
//        Configuration.timeout = 6_000;
        open("https://www.dns-shop.ru/catalog/17a890dc16404e77/smartfony-planshety-i-fototexnika/");
    }

    public static void followingLinks(String... args) {
        for (int arg = 0; arg < args.length; arg++) {
            $(byLinkText(args[arg])).click();
        }
    }
}
